package ru.t1.panasyuk.tm.api;

import ru.t1.panasyuk.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}