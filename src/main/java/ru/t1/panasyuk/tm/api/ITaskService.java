package ru.t1.panasyuk.tm.api;

import ru.t1.panasyuk.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(final String name, final String description);

}