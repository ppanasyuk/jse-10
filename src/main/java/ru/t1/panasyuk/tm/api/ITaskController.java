package ru.t1.panasyuk.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}