package ru.t1.panasyuk.tm.util;

public interface FormatUtil {

    long KILOBYTE = 1024;

    long MEGABYTE = KILOBYTE * 1024;

    long GIGABYTE = MEGABYTE * 1024;

    long TERABYTE = GIGABYTE * 1024;

    static String formatBytes(long bytes) {
        if ((bytes >= 0) && (bytes < KILOBYTE)) {
            return bytes + " B";
        } else if ((bytes >= KILOBYTE) && (bytes < MEGABYTE)) {
            return String.format("%.2f KB", (double) bytes / KILOBYTE);
        } else if ((bytes >= MEGABYTE) && (bytes < GIGABYTE)) {
            return String.format("%.2f MB", (double) bytes / MEGABYTE);
        } else if ((bytes >= GIGABYTE) && (bytes < TERABYTE)) {
            return String.format("%.2f GB", (double) bytes / GIGABYTE);
        } else if (bytes >= TERABYTE) {
            return String.format("%.2f TB", (double) bytes / TERABYTE);
        } else {
            return bytes + " Bytes";
        }
    }

}